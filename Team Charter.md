Team charter / Contrato de equipo
Last updated / Última actualización
2021-09-24
Group name / Nombre del Equipo 
Murderous Mujeres
Roles / Roles


Technical leader / Líder técnicx : Victoria Gutierrez
Submitter / Encargadx de envíos: Isabella Baker


Team Members / Integrantes del equipo


* Victoria Gutierrez, SHE/HER/HERS y ELLA, Argentina, Technical leader


* Isabella Baker, SHE/HER/HERS y ELLA, United States, Submitter


* Melina Comas, SHE/HER/HERS y ELLA, Argentina 


Goals 
1. We want to become proficient at using both VS Code and GitLab.
   1. As a group, we want to learn enough code to be able to create a project that is easy to understand, but still professional quality. This means that we are able to use the skills we learned to potentially create another digital humanities project in the future.
2. We want to be able to engage in cultural exchange by creating a comfortable atmosphere for both Spanish and English to be spoken.
   1. In order to collaborate in a multilingual environment, we need to be able to speak multiple languages. This allows everyone's unique ideas to be heard.
3. We want to learn to be organized during the website-building process by communicating with each other.
   1. Organization doesn’t exist without constant communication. All group members should know what the other members are doing during the course of the project.
Objetivos
1.  Esperamos volvernos competentes en los programas VS Code y Gitlab.
1. Como grupo, queremos aprender lo suficiente de coding para poder hacer un proyecto que sea entendible para todos, pero que mantenga la profesionalidad necesaria. Esto también nos potencia a utilizar dichos recursos para proyectos en humanidades digitales a futuro.
2.  Queremos proveer de un buen intercambio cultural mediante la creación de un ambiente cómodo tanto para los hablantes de inglés como de español.
1. Para poder colaborar en un ambiente bilingüe es necesario hablar ambos idiomas. Así, podremos expresar y escuchar las ideas de todos.
3. Buscamos aprender mediante la comunicación entre nosotras durante el proceso de creación de página web. 
1. Organización no existe sin comunicación. Todos los miembros del grupo deben saber lo que los demás están haciendo durante el proyecto.
Core Values 
1. Open communication  
   1. Don’t be afraid to give respectful criticism. Let your ideas be known.
2. Helping each other when in doubt
   1. Let no one fall behind!
3. Respect 
   1. Be sensitive to cultural differences and a variety of ideas.
4. Patience 
   1. We are in different time zones, and schedules will often conflict. That’s okay, we will work it out.
Fundamentos
1. Comunicación abierto
   1. No tengas miedo de dar críticas respetuosas. Deja que tus ideas sean conocidas.
2. Ayudando cuando alguien tiene dudas
   1. ¡Que nadie se quede atrás!
3. Respeto
   1. Sea sensible a las diferencias culturales y a una variedad de ideas.
4. Paciencia
   1. Estamos en diferentes zonas horarias, y los horarios a menudo entran en conflicto. Está bien, lo resolveremos.
Team management 
1. We will use Zoom and WhatsApp to communicate. We’ll meet once a week for two hours (at least towards the beginning of the semester). 
2. When a decision needs to be made, we will talk it out. This might include a debate, but we would like to strive for all team members to agree/be on the same page.
3. Conflicts should be dealt with the same way as decisions.
4. We are willing to switch roles, especially if someone needs help. No one needs to stick to one role.
Gestión del equipo
1. Usaremos Zoom y WhatsApp para comunicarnos. Nos reunimos una vez cada semana por dos horas.
2. Cuando sea necesario tomar una decisión, la hablaremos. Esto podría incluir un debate, pero nos gustaría esforzarnos para que todos los miembros del equipo estén de acuerdo / estén en la misma página.
3. Los conflictos deben tratarse de la misma manera que las decisiones.
4. Estamos dispuestos a cambiar de rol, especialmente si alguien necesita ayuda. Nadie necesita apegarse a un rol.
Ethos
1. Communication is key.  
2. When in doubt, talk it out. 
3. Enjoy cultural differences.  
4. Make an effort to learn from each other. 
Ethos
1. La comunicación entre nosotras es clave.
2. Sí dudamos, tenemos que decirlo.
3. Disfrutar el intercambio cultural.
4. Esforcémonos en aprender de la otra persona.
