const basePath = '/murderous-mujeres'

module.exports = {
  pathPrefix: basePath,
  siteMetadata: {
    title: `murderous-mujeres`,
    description: `A Digital Humanities group project`,
    author: `Isabella Baker and Victoria Gutierrez`
  },
  plugins: [
    `gatsby-plugin-material-ui`,
    `gatsby-theme-ceteicean`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/tei`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/pages`,
        name: `html`,
      },
    },
  ],
}
